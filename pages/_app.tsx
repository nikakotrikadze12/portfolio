import { AppProps } from "next/app";
import Head from "next/head";
import Layout from "../components/Layout/Layout";
import { Box } from "@mui/material";
import styles from "../styles/Home.module.scss";
import "../styles/styles.css";

export default function CustomApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Nika Kotrikadze | Portfolio</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        ></meta>
      </Head>

      <Box className={styles.container}>
        <Layout />
      </Box>
    </>
  );
}
