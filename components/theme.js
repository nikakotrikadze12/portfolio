import { createMuiTheme } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const theme = createMuiTheme({
  typography: {
    // Override default classes

    body1: {
      margin: 0,
    },
    // Add your other custom typography options here
  },
});
