import React from "react";
import { Box, Typography } from "@mui/material";
import stackStyles from "./stackStyles.module.css";
import { myStack } from "../../constants";
import Image from "next/image";
import { motion } from "framer-motion";
import { fadeIn, textVariant } from "../../libs/motion";
import { staggerContainer } from "../../libs/motion";
//fonts
import { Kanit } from "@next/font/google";
import { Oswald } from "@next/font/google";
import { Roboto } from "@next/font/google";
//logo images
import pymeLogo from "../../assets/pymeLogo.png";
import Link from "next/link";
import { ProjectListInterface, projectList } from "./projectList";

const roboto = Roboto({
  subsets: ["latin"],
  weight: "500",
  display: "swap",
});

const kanit = Kanit({
  subsets: ["latin"],
  weight: "500",
  display: "swap",
});

const oswald = Oswald({
  subsets: ["latin"],
  weight: "700",
  display: "swap",
});

const StackComponent = () => {
  return (
    <motion.section
      variants={staggerContainer(0, 0)}
      initial="hidden"
      whileInView="show"
      viewport={{ once: false, amount: 0.25 }}
    >
      <Box className={stackStyles.stackComponent} id="stackAndSkillsSection">
        <motion.div variants={textVariant(0)}>
          <Typography
            sx={{
              paddingTop: "100px",
              fontSize: ["30px", "40px"],
              color: "white",
              fontFamily: kanit.style,
            }}
          >
            Stack & Skills
          </Typography>
        </motion.div>
        <Box className={stackStyles.stackContainer}>
          <motion.div variants={fadeIn("up", "spring", 0.5, 0.75)}>
            <Box className={stackStyles.wheel}>
              <Box className={stackStyles.glassyRoundBox}>
                <Typography
                  sx={{
                    fontSize: ["30px", "40px", "50px", "60px"],
                    fontFamily: oswald.style,
                  }}
                >
                  Stack
                </Typography>
              </Box>
              <Box className={stackStyles.spokeWrapper}>
                {myStack.map((stack, index) => {
                  const boxClass = `spoke-${index + 1}`;
                  return (
                    <Box
                      className={`${stackStyles.spoke} ${stackStyles[boxClass]}`}
                      key={stack.name}
                    >
                      <Image
                        className={stackStyles.spokeImage}
                        src={stack.imgSrc}
                        alt={stack.name}
                      />
                    </Box>
                  );
                })}
              </Box>
            </Box>
          </motion.div>
          <Box style={{ display: "flex", flexDirection: "column", gap: 50 }}>
            {projectList.map((project: ProjectListInterface) => {
              return (
                <motion.div variants={fadeIn("up", "spring", 1, 0.75)}>
                  <Box className={stackStyles.stackExperienceBox}>
                    <Typography
                      sx={{
                        color: "rgba(47, 17, 87, 0.94)",
                        fontFamily: roboto.style,
                      }}
                    >
                      {project.workingDate}
                    </Typography>
                    <Box className={stackStyles.stackExperienceWorkPlaceBox}>
                      <Box className={stackStyles.companyInfoBox}>
                        <Typography
                          sx={{
                            fontFamily: roboto.style,
                            fontSize: 14,
                          }}
                        >
                          {project.position}
                        </Typography>

                        <Box
                          style={{
                            display: "flex",
                            alignItems: "center",
                            gap: 15,
                          }}
                        >
                          <Typography
                            sx={{
                              fontFamily: roboto.style,
                              fontSize: "16px",
                              color: "rgba(206, 169, 255, 0.94)",
                            }}
                          >
                            {project.companyName}
                          </Typography>
                          <Link
                            href={project.companyLink}
                            target="_blank"
                            className={stackStyles.ImageLink}
                          >
                            <Box
                              style={{
                                backgroundColor: "#60007A",
                                display: "flex",
                                alignItems: "center",
                                padding: "10px 15px",
                                borderRadius: 50,
                              }}
                            >
                              <Image
                                src={project.companyLogo}
                                alt={project.companyName}
                                width={50}
                              />
                            </Box>
                          </Link>
                        </Box>
                      </Box>

                      <br></br>

                      {project.descriptionList.map((description) => (
                        <Typography
                          sx={{
                            fontFamily: roboto.style,
                            width: "300px",
                            height: "auto",
                            fontSize: "13px",
                            color: "rgba(239, 226, 255, 0.94)",
                          }}
                        >
                          {description}
                        </Typography>
                      ))}
                    </Box>
                  </Box>
                </motion.div>
              );
            })}
          </Box>
        </Box>
      </Box>
    </motion.section>
  );
};

export default StackComponent;
