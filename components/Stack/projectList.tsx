import pymeLogo from "../../assets/pymeLogo.png";
import callappLogo from "../../assets/callappLogo.png";
import crocobetLogo from "../../assets/crocLogo.png";

export interface ProjectListInterface {
  companyName: string;
  companyLink: string;
  position: string;
  descriptionList: string[];
  workingDate: string;
  companyLogo: string | any;
}

export const projectList: ProjectListInterface[] = [
  {
    companyName: "Pyme",
    companyLink: "https://www.pymedao.com/",
    position: "Full Stack Developer",
    descriptionList: [
      `• Main objective in the company as a Developer was to find any unmaintained bugs and resolve them or implement any given logic on the website. Most of the time I worked on React.js with some component libraries such as Chakra UI, Ant Design & Material UI. For the database and back end development I worked with firebase and node.js.`,
      `• I was also on a customer support team in discord helping and guiding users through any occurred problems through Firefoo.`,
    ],
    workingDate: "09.2022 - 12.2022",
    companyLogo: pymeLogo,
  },
  {
    companyName: "Callapp",
    companyLink: "https://callapp.ge/",
    position: "Frontend Developer",
    descriptionList: [
      `• Successfully developed and maintained 5+ independent web pages within the company's CRM system using React.js, Zustand, and Ant Design.`,
      `• Collaborated closely with UI/UX designers to ensure seamless translation of design concepts into visually appealing and highly functional user interfaces.`,
      `• Worked in tandem with backend developers to ensure efficient integration of frontend components with the server-side logic.`,
      `• Demonstrated strong problem-solving skills by identifying and resolving frontend issues promptly and effectively.`,
      `• Actively participated in cross-functional discussions, contributing innovative ideas to enhance the overall user experience and efficiency of the CRM system.`,
      `• Stayed updated with the latest industry trends, best practices, and emerging technologies to continually improve development skills and processes.`,
    ],
    workingDate: "07.2023 - 10.2023",
    companyLogo: callappLogo,
  },
  {
    companyName: "Crocobet",
    companyLink: "https://crocobet.com/",
    position: "Service Monitoring Specialist",
    descriptionList: [
      `• Monitor and ensure high-quality and timely inputs for projects.`,
      `• Oversee that projects align with their strategic vision and achieve intended outputs.`,
      `• Facilitate cost-effective and timely completion of project activities.`,
    ],
    workingDate: "03.2024 - 08.2024",
    companyLogo: crocobetLogo,
  },
];
