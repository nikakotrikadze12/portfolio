import React, { useState, useEffect } from "react";
import navStyles from "./navStyles.module.css";
import Link from "next/link";
import { NextRouter } from "next/router";

// MUI icons
import ContactMailIcon from "@mui/icons-material/ContactMail";
import HomeIcon from "@mui/icons-material/Home";
import HomeRepairServiceIcon from "@mui/icons-material/HomeRepairService";
import TerminalIcon from "@mui/icons-material/Terminal";

//fonts
import { Ubuntu } from "@next/font/google";
const ubuntu = Ubuntu({
  subsets: ["latin"],
  weight: "700",
  display: "swap",
});
const HamburgerBar = () => {
  const [isActive, setIsActive] = useState(false);

  const handleClick = () => {
    setIsActive(!isActive);
  };

  useEffect(() => {
    const handleScroll = () => {
      if (isActive) {
        setIsActive(false);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [isActive]);

  interface NavLinksProps {
    href: string | NextRouter;
    name: string;
    icon: JSX.Element;
  }

  const NAVLINKS: NavLinksProps[] = [
    { href: "#homePageSection", name: "Home", icon: <HomeIcon /> },
    {
      href: "#projectsPortfolioSection",
      name: "Projects",
      icon: <HomeRepairServiceIcon />,
    },
    {
      href: "#stackAndSkillsSection",
      name: "Stack",
      icon: <TerminalIcon />,
    },
    {
      href: "#contactMeSection",
      name: "Contact",
      icon: <ContactMailIcon />,
    },
  ];

  const handleScroll = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    // first prevent the default behavior
    e.preventDefault();
    // get the href and remove everything before the hash (#)
    const href = e.currentTarget.href;
    const targetId = href.replace(/.*\#/, "");
    // get the element by id and use scrollIntoView
    const elem = document.getElementById(targetId);
    elem?.scrollIntoView({
      behavior: "smooth",
    });
  };

  return (
    <>
      <div
        className={`${navStyles.sidebar} ${isActive ? navStyles.active : ""}`}
      >
        <div className={navStyles.sidebarContent}>
          <nav className={navStyles.navMenu}>
            <ul className={navStyles.navList}>
              {NAVLINKS.map((val) => (
                <Link
                  className={navStyles.navLink}
                  href={val.href}
                  onClick={handleScroll}
                >
                  <li className={`${navStyles.navItem} ${ubuntu.className}`}>
                    {val.icon}
                    {val.name}
                  </li>
                </Link>
              ))}
            </ul>
          </nav>
        </div>
      </div>
      <svg
        className={`${navStyles.ham} ${navStyles.hamRotate} ${navStyles.ham8} ${
          isActive ? navStyles.active : ""
        }`}
        viewBox="0 0 100 100"
        width="80"
        onClick={handleClick}
      >
        <path
          className={`${navStyles.line} ${navStyles.top}`}
          d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"
        />
        <path
          className={`${navStyles.line} ${navStyles.middle}`}
          d="m 30,50 h 40"
        />
        <path
          className={`${navStyles.line} ${navStyles.bottom}`}
          d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"
        />
      </svg>
    </>
  );
};

export default HamburgerBar;
