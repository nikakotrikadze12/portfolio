import React from "react";
import navStyles from "./navStyles.module.css";
import Image from "next/image";
import LOGO from "../../assets/LOGO.png";

//material UI imports
import { Box } from "@mui/material";
import { Typography } from "@mui/material";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import Slide from "@mui/material/Slide";
import AppBar from "@mui/material/AppBar";
import HamburgerBar from "./HamburgerBar";

//fonts
import { Roboto_Condensed } from "@next/font/google";
const roboto_condensed = Roboto_Condensed({
  subsets: ["latin"],
  weight: "400",
  display: "swap",
});

interface Props {
  window?: () => Window;
  children: React.ReactElement;
}

function HideOnScroll(props: Props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
  });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

const isBrowser = () => typeof window !== "undefined"; //The approach recommended by Next.js

function scrollToTop() {
  if (!isBrowser()) return;
  window.scrollTo({ top: 0, behavior: "smooth" });
}

const Navbar: React.FC = (props: Props) => {
  return (
    <React.Fragment>
      <AppBar>
        <HideOnScroll {...props}>
          <Box className={navStyles.navBox}>
            <Box onClick={scrollToTop} className={navStyles.LogoLink}>
              <Image src={LOGO} alt={"logo"} className={navStyles.logoImage} />
            </Box>
            <Box className={navStyles.navTextStyles}>
              <Typography
                sx={{
                  fontFamily: roboto_condensed.style,
                  fontSize: ["12px", "14px"],
                }}
              >
                +995 597 - 77 - 26 - 76
              </Typography>
              <Typography
                sx={{
                  fontFamily: roboto_condensed.style,
                  fontSize: ["12px", "14px"],
                }}
              >
                nikakotrikadze12@gmail.com
              </Typography>
            </Box>
            <Box>
              <HamburgerBar />
            </Box>
          </Box>
        </HideOnScroll>
      </AppBar>
    </React.Fragment>
  );
};

export default Navbar;
