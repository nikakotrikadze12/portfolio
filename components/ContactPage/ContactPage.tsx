import React, { useState } from "react";
import { Box, Button, Typography } from "@mui/material";
import contactPageStyles from "./contactPageStyles.module.css";
import VolunteerActivismIcon from "@mui/icons-material/VolunteerActivism";
import { motion } from "framer-motion";
import { fadeIn, textVariant } from "../../libs/motion";
import { staggerContainer } from "../../libs/motion";

//fonts
import { Kanit } from "@next/font/google";
import { Roboto } from "@next/font/google";

const roboto = Roboto({
  subsets: ["latin"],
  weight: "500",
  display: "swap",
});

const kanit = Kanit({
  subsets: ["latin"],
  weight: "500",
  display: "swap",
});

const ContactPage = () => {
  const [formSuccess, setFormSuccess] = useState(false);

  interface formDataTypes {
    name: string;
    email: string;
    message: string;
  }

  const [formData, setFormData] = useState<formDataTypes>({
    name: "",
    email: "",
    message: "",
  });
  const [emailError, setEmailError] = useState("");

  const handleInput = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;

    if (fieldName === "email") {
      // Perform email validation
      const emailRegex =
        /^[^\s@]+@(gmail\.com|yahoo\.com|yandex\.ru|mail\.ru|example\.com)$/i;
      if (!emailRegex.test(fieldValue)) {
        setEmailError("Please enter a valid email address");
      } else {
        setEmailError("");
      }
    }

    setFormData((prevState) => ({
      ...prevState,
      [fieldName]: fieldValue,
    }));
  };

  const submitForm = () => {
    const formURL = "https://www.formbackend.com/f/b9425b362eb0d1c0";
    const data = new FormData();

    Object.entries(formData).forEach(([key, value]) => {
      data.append(key, value);
    });

    fetch(formURL, {
      method: "POST",
      body: data,
      headers: {
        accept: "application/json",
      },
    }).then(() => {
      setFormData({
        name: "",
        email: "",
        message: "",
      });
      setFormSuccess(true);
    });
  };

  return (
    <motion.section
      variants={staggerContainer(0, 0)}
      initial="hidden"
      whileInView="show"
      viewport={{ once: false, amount: 0.25 }}
    >
      <Box className={contactPageStyles.contactContainer} id="contactMeSection">
        <motion.div variants={textVariant(0)}>
          <Typography
            sx={{
              fontFamily: kanit.style,
              color: "white",
              fontSize: ["30px", "40px"],
              paddingTop: "100px",
              textAlign: "center",
            }}
          >
            Shoot Me a <span style={{ color: "#4739C8" }}>Message</span>
          </Typography>
        </motion.div>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "70%",
            height: ["500px", "600px"],
            backdropFilter: "blur(12px)",
            opacity: 0.3,
            backgroundColor: "rgb(82, 98, 187)",
          }}
        >
          {formSuccess ? (
            <Box>
              <Typography
                sx={{
                  fontFamily: roboto.style,
                  fontSize: ["20px", "40px"],
                  color: " white",
                  textAlign: "center",
                }}
              >
                Thanks! Your message was successfully received
                <br></br>
                <VolunteerActivismIcon sx={{ fontSize: 100 }} />
                <Typography
                  sx={{
                    fontFamily: roboto.style,
                    fontSize: ["13px", "16px"],
                  }}
                >
                  I'll shoot you a message back to your email ASAP
                </Typography>
              </Typography>
            </Box>
          ) : (
            <Box
              sx={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column",
              }}
            >
              <Box
                component="form"
                acceptCharset="UTF-8"
                onSubmit={(e) => {
                  e.preventDefault();
                  submitForm();
                }}
                sx={{
                  display: "flex",
                  alignItems: "center",
                  flexDirection: "column",
                  gap: "50px",
                  width: "70%",
                  color: "white",
                  fontSize: "18px",
                  position: "absolute",
                  zIndex: 1,
                  "@media (max-width: 1000px)": {
                    flexDirection: "column",
                    gap: "30px",
                    width: "60%",
                    fontSize: "14px",
                  },
                }}
              >
                <motion.div
                  style={{ width: "100%" }}
                  variants={fadeIn("up", "spring", 0.5, 0.75)}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      width: "100%",
                      gap: "3px",
                    }}
                  >
                    <label className={roboto.className}>Name</label>
                    <input
                      type="text"
                      name="name"
                      placeholder="Enter your name"
                      maxLength={30}
                      className={contactPageStyles.inputStyle}
                      onChange={handleInput}
                      value={formData.name}
                    />
                  </Box>
                </motion.div>

                <motion.div
                  style={{ width: "100%" }}
                  variants={fadeIn("up", "spring", 1, 0.75)}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      width: "100%",
                      gap: "3px",
                    }}
                  >
                    <label className={roboto.className}>Email</label>
                    <input
                      type="email"
                      name="email"
                      placeholder="Enter your email"
                      maxLength={30}
                      className={contactPageStyles.inputStyle}
                      onChange={handleInput}
                      value={formData.email}
                    />
                    {emailError && formData.email.length > 0 && (
                      <Typography
                        sx={{
                          fontFamily: roboto.style,
                          fontSize: ["12px", "14px"],
                          color: "rgb(255, 255, 255)",
                        }}
                      >
                        {emailError}
                      </Typography>
                    )}
                    {formData.email.length == 0 ? (
                      <Typography
                        sx={{
                          fontFamily: roboto.style,
                          fontSize: ["12px", "14px"],
                          color: "rgb(255, 255, 255)",
                        }}
                      >
                        Only receiving @gmail.com @yahoo.com
                      </Typography>
                    ) : (
                      ""
                    )}
                  </Box>
                </motion.div>

                <motion.div
                  style={{ width: "100%" }}
                  variants={fadeIn("up", "spring", 1.5, 0.75)}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      width: "100%",
                      gap: "3px",
                    }}
                  >
                    <label className={roboto.className}>Message</label>
                    <textarea
                      name="message"
                      placeholder="What's on your mind"
                      rows={5}
                      maxLength={300}
                      className={contactPageStyles.textAreaStyle}
                      onChange={handleInput}
                      value={formData.message}
                    ></textarea>
                  </Box>
                </motion.div>
                <motion.div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                  }}
                  variants={fadeIn("up", "spring", 2, 0.75)}
                >
                  <Button
                    type="submit"
                    sx={{
                      fontFamily: roboto.style,
                      fontSize: ["11px", "12px", "14px", "16px"],
                      color: "white",
                      backgroundColor: "blue",
                      width: ["150px", "250px"],
                      "&.Mui-disabled": {
                        color: "#95A4E4",
                        backgroundColor: "#2E4ABF",
                      },
                    }}
                    disabled={
                      formData.name === "" ||
                      formData.email === "" ||
                      formData.message === "" ||
                      emailError != ""
                    }
                  >
                    Send message
                  </Button>
                </motion.div>
              </Box>
            </Box>
          )}
        </Box>
      </Box>
    </motion.section>
  );
};

export default ContactPage;
