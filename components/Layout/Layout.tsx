import React, { useEffect } from "react";
import Navbar from "../Navbar/Navbar";
import HomePage from "../HomePage/HomePage";
import Projects from "../Projects/Projects";
import Box from "@mui/material/Box";
import StackComponent from "../Stack/StackComponent";
import ContactPage from "../ContactPage/ContactPage";
import Footer from "../Footer/Footer";

const Layout: React.FC = () => {
  useEffect(() => {
    window.scrollTo({ top: 0 });
  }, []);
  return (
    <Box>
      <Navbar />
      <HomePage />
      <Projects />
      <StackComponent />
      <ContactPage />
      <Footer />
    </Box>
  );
};

export default Layout;
