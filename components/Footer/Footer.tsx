import React from "react";
import footerStyles from "./footerStyles.module.css";
import { Box, Typography } from "@mui/material";
import Image from "next/image";
import Logo from "../../assets/LOGO.png";
import Divider from "@mui/material/Divider";
import { NextRouter } from "next/router";
import Link from "next/link";

//Icons
import LinkedInIcon from "../../assets/linkedInIcon.svg";
import GitHubIcon from "../../assets/githubIcon.svg";
import GitLubIcon from "../../assets/gitlabIcon.svg";
import CopyrightIcon from "@mui/icons-material/Copyright";

//framer motion
import { motion } from "framer-motion";
import { fadeIn } from "../../libs/motion";
import { staggerContainer } from "../../libs/motion";

//fonts
import { Ubuntu } from "@next/font/google";
import { Roboto } from "@next/font/google";
const ubuntu = Ubuntu({
  subsets: ["latin"],
  weight: "500",
  display: "swap",
});
const roboto = Roboto({
  subsets: ["latin"],
  weight: "500",
  display: "swap",
});

const Footer = () => {
  interface NavLinksProps {
    href: string | NextRouter;
    name: string;
    icon: JSX.Element;
  }

  const NAVLINKS: NavLinksProps[] = [
    {
      href: "https://www.linkedin.com/in/nikoloz-k-515a921aa/",
      name: "LinkedIn",
      icon: <Image src={LinkedInIcon} alt={"linkedin-icon"} width={25} />,
    },
    {
      href: "https://github.com/nikaKotrikadze",
      name: "GitHub",
      icon: <Image src={GitHubIcon} alt={"github-icon"} width={25} />,
    },
    {
      href: "https://gitlab.com/nikakotrikadze12",
      name: "GitLab",
      icon: <Image src={GitLubIcon} alt={"gitlab-icon"} width={25} />,
    },
  ];
  return (
    <motion.section
      variants={staggerContainer(0, 0)}
      initial="hidden"
      whileInView="show"
      viewport={{ once: false, amount: 0.25 }}
    >
      <Box className={footerStyles.FooterContainer}>
        <Box className={footerStyles.footerContentBox}>
          <Box className={footerStyles.footerLogoBox}>
            <motion.div variants={fadeIn("up", "spring", 0.5, 1)}>
              <Image src={Logo} alt="logo" className={footerStyles.logoImage} />
            </motion.div>
          </Box>

          <Box className={footerStyles.footerSocialsBox}>
            <Divider
              orientation="vertical"
              variant="middle"
              flexItem
              style={{ background: "white" }}
            />
            <ul className={footerStyles.navList}>
              {NAVLINKS.map((val, index) => (
                <motion.div
                  variants={fadeIn("up", "spring", index * 0.5, 0.75)}
                >
                  <Link
                    className={footerStyles.navLink}
                    href={val.href}
                    target="_blank"
                  >
                    <li
                      className={`${footerStyles.navItem} ${ubuntu.className}`}
                    >
                      {val.icon}
                      {val.name}
                    </li>
                  </Link>
                </motion.div>
              ))}
            </ul>
          </Box>
        </Box>
        <motion.div variants={fadeIn("up", "spring", 0.5, 0.75)}>
          <Box className={footerStyles.copyrightBox}>
            <CopyrightIcon />
            <Typography
              sx={{
                fontFamily: roboto.style,
                fontSize: ["12px", "14px"],
              }}
            >
              2023 Nika Kotrikadze
            </Typography>
          </Box>
        </motion.div>
      </Box>
    </motion.section>
  );
};

export default Footer;
