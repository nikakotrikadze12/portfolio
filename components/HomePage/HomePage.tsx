import React from "react";
import homeStyles from "./homeStyles.module.css";
import { Box, Typography } from "@mui/material";
import MyImg from "../../assets/nika.jpg";
import Image from "next/image";
import { motion } from "framer-motion";
import { fadeIn } from "../../libs/motion";
import { staggerContainer } from "../../libs/motion";
import PageDevider from "./PageDevider/PageDevider";

//fonts
import { Roboto } from "@next/font/google";
import { Kanit } from "@next/font/google";

const roboto = Roboto({
  subsets: ["latin"],
  weight: "500",
  display: "swap",
});

const kanit = Kanit({
  subsets: ["latin"],
  weight: "500",
  display: "swap",
});

const HomePage: React.FC = () => {
  return (
    <motion.section
      variants={staggerContainer(0, 0)}
      initial="hidden"
      whileInView="show"
      viewport={{ once: false, amount: 0.25 }}
    >
      <Box className={homeStyles.homeContainer} id="homePageSection">
        <Box className={homeStyles.homeContent}>
          <Box>
            <motion.div variants={fadeIn("up", "spring", 0.5, 0.75)}>
              <Typography
                sx={{
                  fontSize: ["40px", "60px", "90px"],
                  fontFamily: kanit.style,
                }}
              >
                Hello, I'm
              </Typography>
              <Typography
                sx={{
                  fontSize: ["100px", "120px", "150px"],
                  color: "rgb(61, 61, 135)",
                  fontFamily: kanit.style,
                }}
              >
                Nika
              </Typography>
            </motion.div>
            <motion.div variants={fadeIn("up", "spring", 1, 0.75)}>
              <Typography
                sx={{
                  width: ["300px", "400px", "500px"],
                  fontSize: ["12px", "13px"],
                  color: "#e7e4ff",
                  backgroundColor: "transparent",
                  padding: "15px",
                  borderRadius: "20px",
                  boxShadow: "0px 0px 10px rgb(131, 131, 196)",
                  fontFamily: roboto.style,
                }}
              >
                Experienced Front end Developer with 3+ years of industry
                experience, specializing in React and Next.js. Skilled in
                creating dynamic and interactive user interfaces, while
                translating designs into robust web applications. Proficient in
                collaborative development practices and project management tools
                such as Trello. Detail-oriented problem solver with a passion
                for intuitive and user-friendly interfaces, who continuously
                stays updated with the latest industry trends and best
                practices.
              </Typography>
            </motion.div>
          </Box>
          <motion.div variants={fadeIn("up", "spring", 1.5, 1)}>
            <Image
              src={MyImg}
              width={400}
              alt={"niko"}
              className={homeStyles.myImage}
            />
          </motion.div>
        </Box>
      </Box>
      <PageDevider />
    </motion.section>
  );
};

export default HomePage;
