import React from "react";
import { Box, Typography } from "@mui/material";
import LanguageIcon from "@mui/icons-material/Language";
import GitHubIcon from "@mui/icons-material/GitHub";
import projectsStyles from "./projectsStyles.module.css";
import Image from "next/image";
import Link from "next/link";

//fonts
import { Roboto } from "@next/font/google";

const roboto = Roboto({
  subsets: ["latin"],
  weight: "500",
  display: "swap",
});

const ProjectCard = ({
  imageSrc,
  name,
  description,
  githubLink,
  websiteLink,
  index,
}) => {
  return (
    <Box className={projectsStyles.CardContainer}>
      <Box className={projectsStyles.cardImageContainer}>
        <Image alt={name} src={imageSrc} className={projectsStyles.cardImage} />
      </Box>
      <Box className={projectsStyles.cardInfoAfterImage}>
        <Typography
          sx={{
            fontSize: ["16px", "18px"],
            fontFamily: roboto.style,
          }}
        >
          {name}
        </Typography>
        <Typography
          sx={{
            marginTop: "10px",
            padding: "0px 10px 10px 10px",
            fontSize: ["12px", "14px"],
            color: "rgb(192, 163, 183)",
            fontFamily: roboto.style,
          }}
        >
          {description}
        </Typography>
        <Box className={projectsStyles.linkIcons}>
          <Link
            href={websiteLink}
            target={"_blank"}
            className={
              websiteLink.length === 0 || websiteLink == ""
                ? projectsStyles.webLinkError
                : projectsStyles.webLink
            }
          >
            <LanguageIcon />
          </Link>
          <Link
            href={githubLink}
            target={"_blank"}
            className={
              githubLink.length === 0 || githubLink == ""
                ? projectsStyles.webLinkError
                : projectsStyles.webLink
            }
          >
            <GitHubIcon />
          </Link>
        </Box>
      </Box>
    </Box>
  );
};

export default ProjectCard;
