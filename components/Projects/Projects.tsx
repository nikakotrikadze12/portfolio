import React from "react";
import projectsStyles from "./projectsStyles.module.css";
import { Box, Typography } from "@mui/material";

//framer motion imports
import { motion } from "framer-motion";
import { textVariant } from "../../libs/motion";
import { staggerContainer } from "../../libs/motion";
import { fadeIn } from "../../libs/motion";

import { allProjects } from "../../constants";
import ProjectCard from "./ProjectCard";

//fonts
import { Kanit } from "@next/font/google";

const kanit = Kanit({
  subsets: ["latin"],
  weight: "500",
  display: "swap",
});
const Projects = () => {
  return (
    <Box
      className={projectsStyles.ProjectsContainer}
      id="projectsPortfolioSection"
    >
      <motion.section
        variants={staggerContainer(0, 0)}
        initial="hidden"
        whileInView="show"
        viewport={{ once: false, amount: 0.1 }}
      >
        <motion.div variants={textVariant(0)}>
          <Box className={projectsStyles.headTitleBox}>
            <Typography
              sx={{
                paddingTop: "100px",
                paddingBottom: "50px",
                fontSize: ["30px", "40px"],
                color: "white",
                fontFamily: kanit.style,
              }}
            >
              Projects Portfolio
            </Typography>
          </Box>
        </motion.div>
      </motion.section>

      <motion.section
        variants={staggerContainer(0, 0)}
        initial="hidden"
        whileInView="show"
        viewport={{ once: false, amount: 0.1 }}
      >
        <motion.div variants={textVariant(0)}>
          <Typography
            sx={{
              textAlign: "left",
              pl: 10,
              fontSize: ["10px", "15px"],
              color: "#FFD700",
              fontFamily: kanit.style,
            }}
          >
            PROJECTS
          </Typography>
        </motion.div>

        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            marginTop: "50px",
          }}
        >
          <Box
            sx={{
              display: "grid",
              gap: "150px",
              gridTemplateColumns: "repeat(4, 1fr)",
              "@media (max-width: 1500px)": {
                gridTemplateColumns: "repeat(3, 1fr)",
              },
              "@media (max-width: 1100px)": {
                gridTemplateColumns: "repeat(2, 1fr)",
              },
              "@media (max-width: 700px)": {
                gridTemplateColumns: "1fr",
              },
            }}
          >
            {allProjects.map((project, index) => {
              return (
                <motion.div variants={fadeIn("up", "spring", 0.5, 0.75)}>
                  <ProjectCard
                    imageSrc={project.imageSrc}
                    name={project.name}
                    description={project.description}
                    githubLink={project.githubLink}
                    websiteLink={project.websiteLink}
                    key={index}
                    index={index}
                  />
                </motion.div>
              );
            })}
          </Box>
        </Box>
      </motion.section>
    </Box>
  );
};

export default Projects;
