//project img imports
import memeImg from "../assets/meme.png";
import onlineMarketImg from "../assets/onlineMarketImg.png";
import typeracer from "../assets/typeracer.png";
import homeBraiding from "../assets/homeBraidingImg.png";
import quizical from "../assets/quizical.png";
import cryptExchange from "../assets/cryptExchange.png";
import wordleHints from "../assets/wordleHints.png";
import jeirani from "../assets/jeirani.png";
import githubSearcherImg from "../assets/githubSearcherImg.png";

//stack img imports
import jsLogo from "../assets/jsLogo.png";
import tsLogo from "../assets/ts.png";
import nextLogo from "../assets/nextLogo.png";
import reactLogo from "../assets/reactLogo.png";
import cssLogo from "../assets/cssLogo.png";
import MUILOGO from "../assets/MUILOGO.png";

export const allProjects = [
  {
    imageSrc: onlineMarketImg,
    name: "e-Commerce website",
    description:
      "online marketplace powered with react & firebase. Has integrated user login and posting system",
    githubLink: "https://github.com/nikaKotrikadze/e-commerce-website-marketik",
    websiteLink: "https://super-marketik1.netlify.app/",
  },
  {
    imageSrc: githubSearcherImg,
    name: "Github Searcher",
    description:
      "The GitHub Searcher project is a web application that allows users to search for GitHub profiles by username. Users can enter a GitHub username in the search input, and the application retrieves and displays information about the corresponding user",
    githubLink: "https://github.com/nikaKotrikadze/github-searcher",
    websiteLink: "https://github-name-search.netlify.app/",
  },
  {
    imageSrc: typeracer,
    name: "Type Racer",
    description:
      "Type fast and keep up with the words that are coming up on the screen",
    githubLink: "https://github.com/nikaKotrikadze/TypeRacer",
    websiteLink: "https://nikakotrikadze-typeracer.netlify.app/",
  },
  {
    imageSrc: memeImg,
    name: "Meme Generator",
    description: "Find and give titles to your favorite memes",
    githubLink: "https://github.com/nikaKotrikadze/Meme-Generator",
    websiteLink: "https://nikakotrikadze-meme-generator.netlify.app/",
  },
  {
    imageSrc: homeBraiding,
    name: "Booking Website",
    description: "Simple booking website for a hair braiding appointment ",
    githubLink: "https://github.com/nikaKotrikadze/react-HomeBraiding-website",
    websiteLink: "https://bookforbraiding.netlify.app/",
  },
  {
    imageSrc: quizical,
    name: "Scrimba Quiz",
    description:
      "Quiz WebApp that access trivia questions Quiz WebApp that access trivia questions from the Open Trivia Database API",
    githubLink: "https://github.com/nikaKotrikadze/Quiziz_scrimba",
    websiteLink: "",
  },
  {
    imageSrc: cryptExchange,
    name: "Crypto Converter",
    description:
      "Instantly calculate and compare rates for a wide range of digital currencies, empowering you to stay up-to-date with the ever-changing crypto market",
    githubLink: "https://github.com/nikaKotrikadze/cryptoExchange",
    websiteLink: "",
  },
  {
    imageSrc: wordleHints,
    name: "Wordle Hints",
    description:
      "Wordle is wonderfully simple. The aim is to guess the correct multi-letter word within couple of guesses",
    githubLink: "https://github.com/nikaKotrikadze/WordleHints",
    websiteLink: "",
  },
  {
    imageSrc: jeirani,
    name: "Je-i-ra-ni",
    description:
      "Jeirani is a national Georgian game with simple rules of 'rock paper scissors', but instead of the 'rock', we use 'pit'",
    githubLink: "https://github.com/nikaKotrikadze/jeiraniGame",
    websiteLink: "",
  },
];

export const myStack = [
  {
    name: "JS",
    imgSrc: jsLogo,
  },
  {
    name: "TS",
    imgSrc: tsLogo,
  },
  {
    name: "Next.js",
    imgSrc: nextLogo,
  },
  {
    name: "React.js",
    imgSrc: reactLogo,
  },
  {
    name: "MUI",
    imgSrc: MUILOGO,
  },
  {
    name: "CSS",
    imgSrc: cssLogo,
  },
];
